import React from "react";
import "./BusinessList.css";

import Business from "../Business/Business";

class BusinessList extends React.Component {
	render() {
		return (
			<div className="BusinessList">
				{/* The next two lines take the data returned from yelp and map it onto the shown parameters in the Business Component */}
				{this.props.businesses.map(business => {
					return <Business business={business} key={business.id} />;
				})}
			</div>
		);
	}
}

export default BusinessList;
