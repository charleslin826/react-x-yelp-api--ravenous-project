import React from "react";
import "./Business.css";

class Business extends React.Component {
	render() {
		return (
			<div className="Business">
				<div className="image-container">
					{/* This shows the default image from the Yelp record. */}
					<img src={this.props.business.imageSrc} alt="" />
				</div>
				{/* This is the name of the business as shown in the Yelp listing. */}
				<h2>{this.props.business.name}</h2>

				{/* The div below contains: 
              Column 1: address 
              Column 2category, average rating, and number of reviews.*/}
				<div className="Business-information">
					{/* Column 1: Address*/}
					<div className="Business-address">
						<p>{this.props.business.address}</p>
						<p>{this.props.business.city}</p>
						<p>{`${this.props.business.state} ${
							this.props.business.zipCode
						}`}</p>
					</div>

					{/* Column 2: Category, Average Rating, & Number of Reviews*/}
					<div className="Business-reviews">
						<h3>{this.props.business.category.toUpperCase()}</h3>
						<h3 className="rating">{`${
							this.props.business.rating
						} stars`}</h3>
						<p>{`${this.props.business.reviewCount} reviews`}</p>
					</div>
				</div>
			</div>
		);
	}
}

export default Business;
