const apiKey = 'tWPMsnWHy2Tg_QD3DLUPaywSLwFpQHcXfZ4aN8EeYTleeWNGjlEXcNnF4N6E7sKmsSEiPzGj2BY2v7iMb_KV7CveBax7uRUzWAoWvqAXEcnJZOQrJSG7JiMdht6yW3Yx';


/**
 * const Yelp:
 * 1.  takes the search term, location, and sort by parameter from the UI and fetches the results via Yelp's API
 * 2.  it authenticates by adding my apiKey see headers: object in the first return function
 * 3.  it converts the response to json
 * 4.  maps the converted response onto the business object
 */

 
const Yelp = {
  search(term, location, sortBy) {
    return fetch(`https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/businesses/search?term=${term}&location=${location}&sort_by=${sortBy}`, {
      headers: {
        Authorization: `Bearer ${apiKey}`
      }
  }).then(response => {
    return response.json();
  }).then(jsonResponse => {
    if (jsonResponse.businesses) {
      console.log(jsonResponse.businesses);
      return jsonResponse.businesses.map(business => ({
          id: business.id,
          imageSrc: business.image_url,
          name: business.name,
          address: business.location.address1,
          city: business.location.city,
          state: business.location.state,
          zipCode: business.location.zip_code,
          category: business.categories[0].title,
          rating: business.rating,
          reviewCount: business.review_count
        }
      ));
    }
  });
  }
};

export default Yelp;
